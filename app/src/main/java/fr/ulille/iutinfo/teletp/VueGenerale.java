package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.ArrayList;
import java.util.List;

public class VueGenerale extends Fragment {

    String salle;
    String poste;
    String DISTANCIEL;
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = this.DISTANCIEL;
        this.model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        Spinner spinner = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                update(view);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                update(view);
            }

        });

        spinner = (Spinner) view.findViewById(R.id.spPoste);
        adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText text = (EditText)view.findViewById(R.id.tvLogin);
            String value = text.getText().toString();
            model.setUsername(((EditText) view.findViewById(R.id.tvLogin)).getText().toString());

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        this.update(view);

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    void update(View view){
        Spinner salle = (Spinner) view.findViewById(R.id.spSalle);
        Spinner postes = (Spinner) view.findViewById(R.id.spPoste);

        String salleValue = salle.getSelectedItem().toString();

        if(salleValue.equals("Distanciel")){
            postes.setVisibility(View.GONE);
            postes.setEnabled(false);
            model.setLocalisation("Distanciel");
        }
        else{
            postes.setVisibility(View.VISIBLE);
            postes.setEnabled(true);
            model.setLocalisation( salleValue+" : "+postes.getSelectedItem().toString());
        }
    }
    // TODO Q9
}